import pandas as pd
import numpy as np
import scipy
import scipy.stats
import sklearn
from xgboost import XGBRegressor
from sklearn.ensemble import RandomForestRegressor
import load_data
import generate_features
from crossvalidation import timeMonthSplit
import json
import argparse
import gc

month_name = ['January', 'February', 'March', 'April', 'May', 'June',
              'July', 'August',	'September', 'October',	'November',	'December']

xgboost_params = {'learning_rate': 0.10000000000000001, 'max_depth': 8, 'n_estimators': 50, 'min_child_weight': 0,
                  'reg_lambda': 0.518152548941889, 'reg_alpha': 0, 'subsample': 0.82914690736714158, 'n_jobs': -1,
                  'random_state': 1}

xgboost_params_distribution = {
    'max_depth': scipy.stats.randint(4, 10),
    'learning_rate': [0.1, 0.01],
    'n_estimators': [50, 100],
    'min_child_weight': [0, 5, 15],
    'reg_alpha': 0,
    'reg_lambda': scipy.stats.uniform(0, 1),
    'subsample': scipy.stats.uniform(0, 1),
    'random_state': 1,
    'n_jobs': -1}


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NumpyEncoder, self).default(obj)


def train(X, Y, params):
    #regr = RandomForestRegressor(**params)
    regr = XGBRegressor(**params)
    #print(X.head())
    regr.fit(X, Y)
    return regr


def train_crossvalidation(data, data_features, params=None, n_splits=4, suffix=None):
    rmses = []
    for train_idx, val_idx in timeMonthSplit(n_splits, data):
        X = data_features[train_idx]
        Y = data['item_cnt_month'][train_idx]
        X_val = data_features[val_idx]
        Y_val = data['item_cnt_month'][val_idx]
        month = data[val_idx]['date_block_num'].min()
        print("Date_block_num", month, month_name[month % 12])

        regr = train(X, Y, params)
        # validation scores
        Y_pred = regr.predict(X_val)

        if suffix is not None:
            np.savetxt('data/predictions/valid_pred_' + str(month) + '_' + suffix + '.csv', Y_pred)
        rmse = np.sqrt(((Y_val - Y_pred)**2).mean())
        print('Valid RMSE ', rmse)
        rmses.append(rmse)
        if rmse > 0.93:
            print('Bad parameters, skipping')
            break
        # training scores
        Y_pred = regr.predict(X)
        rmse = np.sqrt(((Y - Y_pred)**2).mean())
        print('Train RMSE ', rmse)
        #print(list(X))
        #print(regr.feature_importances_)
    return np.mean(rmses)


def choose_params(param_distributions):
    params = {}
    for key, value in param_distributions.items():
        if isinstance(value, (list, tuple)):
            v = np.random.choice(value)
        elif hasattr(value, 'rvs'):
            v = value.rvs()
        else:
            v = value
        params[key] = v
    return params


def parameter_search(data, data_features, param_distributions, n_iter=10, n_splits=5):
    best_score = None
    best_params = {}
    for i in range(n_iter):
        params = choose_params(param_distributions)
        print('Checking params')
        print(params)
        score = train_crossvalidation(data, data_features, params, n_splits=n_splits)
        print('Score: %f' % score)
        if best_score is None:
            best_params = params
            best_score = score
        elif score < best_score:
            best_params = params
            best_score = score
            print('Best params')
    return best_params



if __name__ == '__main__':
    suffix = 'xgb'
    parser = argparse.ArgumentParser()
    parser.add_argument('--param_search', action='store_true')
    parser.add_argument('--no_crossval', action='store_true')
    args = parser.parse_args()
    data = load_data.load_data()
    gc.collect()

    test_data = pd.read_csv("data/test.csv")
    test_data['date_block_num'] = data['date_block_num'].max() + 1
    selection = test_data[['date_block_num', 'shop_id', 'item_id']]
    X, X_test = generate_features.generate_features(data, selection)

    good_dates_mask = data['date_block_num'] > 12
    data = data[good_dates_mask]
    X = X[good_dates_mask]

    n_splits = 5
    rmse = 0.0
    if args.param_search:
        param_distributions = {'n_estimators': 20,
                               'max_features': None,
                               'max_depth': scipy.stats.randint(10, 20) ,
                               'min_samples_leaf': scipy.stats.randint(4, 20),
                               'random_state': 1,
                               'n_jobs': -1}

        param_distributions = xgboost_params_distribution

        np.random.seed(param_distributions['random_state'])
        n_iter = 20
        print('Searching parameters with n_iter=%d, n_splits=%d' % (n_iter, n_splits))
        params = parameter_search(data, X, param_distributions, n_iter=n_iter, n_splits=n_splits)
        print('Best parameters found:')
        print(params)
        with open('hyper-params.txt', 'w') as f:
            f.write(json.dumps(params, cls=NumpyEncoder))
    else:
        try:
            f = open('hyper-params.txt')
            params = json.loads(f.read())
            print('Params loaded from hyper-params.txt')
        except IOError:
            params = xgboost_params
            #params = {'n_estimators': 20, 'max_features': None, 'max_depth': 14, 'min_samples_leaf': 8, 'random_state': 1, 'n_jobs': -1}
            print('Use default params')
            with open('hyper-params.txt', 'w') as f:
                f.write(json.dumps(params, cls=NumpyEncoder))
    if not args.no_crossval:
        print('Doing crossvalidation with %d splits' % n_splits)
        rmse = train_crossvalidation(data, X, params, n_splits=n_splits, suffix=suffix)
        print('\nAvrg RMSE', rmse)

    Y = data['item_cnt_month']
    regr = train(X, Y, params)
    Y_pred = regr.predict(X_test)

    test_data['item_cnt_month'] = Y_pred
    submission = test_data[['ID', 'item_cnt_month']]
    fname = 'data/submission_%s_%f.csv' % (suffix, rmse)
    submission.to_csv(fname, index=False)
    print('Done')
    print('Submission written to ' + fname)
    pass
