import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


def load_data(cache=True):
    if cache:
        try:
            data = pd.read_hdf('data/train.h5')
            print('Data loaded from cache')
            return data
        except:
            pass

    print('Load data from csv')
    sales = pd.read_csv("data/sales_train.csv.gz", dtype={'item_id': np.int32, 'shop_id': np.int32, 'date_block_num': np.int32, 'item_price': np.float32})
    sales.date = pd.to_datetime(sales.date, format='%d.%m.%Y')
    grouped = sales.groupby(['date_block_num', 'shop_id', 'item_id'])
    sales = grouped.agg({'item_price': np.mean, 'item_cnt_day': np.sum})
    sales['item_cnt_day'] = sales['item_cnt_day'].clip(0, 20)
    sales.rename(columns={'item_cnt_day':'item_cnt_month'}, inplace=True)

    idx = []
    for date_block in sales.index.get_level_values('date_block_num').unique():
        shop_ids = sales.loc[date_block:date_block].index.get_level_values('shop_id').unique()
        item_ids = sales.loc[date_block:date_block].index.get_level_values('item_id').unique()
        idx_part = pd.MultiIndex.from_product([[date_block], shop_ids, item_ids])
        idx.extend(idx_part.tolist())
        pass
    idx = pd.MultiIndex.from_tuples(idx, names=('date_block_num', 'shop_id', 'item_id'))
    data = pd.DataFrame(index=idx, columns=sales.columns)
    data.loc[sales.index] = sales

    data['item_cnt_month'].fillna(0, inplace=True)
    data["item_price"] = data["item_price"].groupby("item_id").transform(lambda x: x.fillna(x.mean()))

    data.reset_index(inplace=True)

    items = pd.read_csv("data/items.csv", dtype={'item_id': np.int32, 'item_category_id': np.int32})
    items.drop(columns=['item_name'], inplace=True)
    #item_categories = pd.read_csv("data/item_categories.csv")
    #shops = pd.read_csv("data/shops.csv")
    #data = data.join(shops.set_index('shop_id'), on='shop_id')
    data = data.join(items.set_index('item_id'), on='item_id')
    #data = data.join(item_categories.set_index('item_category_id'), on='item_category_id')

    data.to_hdf('data/train.h5', 'w')
    return data


def group_data(data):
    grouped = data.groupby(data.date.dt.to_period('M'))
    last_month = pd.Period('2015-10')
    month_data = grouped.get_group(last_month)
    grouped = month_data.groupby(['shop_id', 'item_id'])
    aggregated = grouped.agg({'item_cnt_day': np.sum})
    return aggregated


def create_first_submission():
    train_data = load_data()
    grouped = train_data.groupby('date_block_num')
    train_data = grouped.get_group(train_data.date_block_num.max())
    train_data.reset_index(inplace=True)

    test_data = pd.read_csv("data/test.csv")

    test_data = test_data.join(train_data.set_index(['shop_id', 'item_id']), on=['shop_id', 'item_id'])
    submission = test_data[['ID', 'item_cnt_month']]
    submission.fillna(0, inplace=True)
    submission.to_csv('data/baseline_submission.csv', index=False)
    return submission