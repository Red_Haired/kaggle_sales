import pandas as pd
import numpy as np


def timeMonthSplit(n_splits, X, month_field='date_block_num'):
    last_month = X[month_field].max()
    for month in range(last_month, last_month - n_splits, -1): #range(last_month - n_splits + 1, last_month + 1)
        index = (X[month_field] == month)
        train_index = (X[month_field] < month)
        yield train_index, index
