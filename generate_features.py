import pandas as pd
import numpy as np
import os


def mean_encoding(train_data, train_features, val_features, col, target_col='item_cnt_month',
                  name=None, operation=np.mean, lags=(1, 2, 3, 12), duration=1):

    aggregated = train_data.groupby(['date_block_num', col])[target_col].aggregate(operation)
    if name:
        new_col_name_tmpl = name
    else:
        new_col_name_tmpl = col + '_' + operation.__name__ + '_' + target_col[-5:]
    if val_features is not None:
        date_block = val_features['date_block_num'].min()
        for lag in lags:
            means = aggregated.loc[date_block - lag:date_block - lag + duration - 1].groupby(col).aggregate(operation)
            val_features[new_col_name_tmpl + '_' + str(lag)] = val_features[col].map(means)
    for date_block in train_data['date_block_num'].unique():
        val_index = (train_data['date_block_num'] == date_block)
        X_val = train_features[val_index]
        for lag in lags:
            means = aggregated.loc[date_block - lag:date_block - lag + duration - 1].groupby(col).aggregate(operation)

            new_col_name = new_col_name_tmpl + '_' + str(lag)

            X_val[new_col_name] = X_val[col].map(means)
            if new_col_name not in list(train_features):
                train_features[new_col_name] = None
        train_features[val_index] = X_val
    # fill nans
    mean = 0#train_data[target_col].mean()
    #print(mean)
    for lag in lags:
        new_col_name = new_col_name_tmpl + '_' + str(lag)
        train_features[new_col_name].fillna(mean, inplace=True)
        if val_features is not None:
            val_features[new_col_name].fillna(mean, inplace=True)


def generate_shop_features(train_data, train_features, val_features=None):
    mean_encoding(train_data, train_features, val_features, 'shop_id')
    return train_features, val_features


def generate_item_features(train_data, train_features, val_features):

    grouped = train_data.groupby('item_id')
    mean_encoding(train_data, train_features, val_features, col='item_id')
    mean_encoding(train_data, train_features, val_features, col='item_id', target_col='item_price')

    items = pd.read_csv("data/items.csv")
    items.drop(columns=['item_name'], inplace=True)
    train_features = train_features.join(items.set_index('item_id'), on='item_id')
    if val_features is not None:
        val_features = val_features.join(items.set_index('item_id'), on='item_id')
    mean_encoding(train_data, train_features, val_features, col='item_category_id')
    #items = grouped.agg({'item_price': np.mean, 'item_cnt_month': np.mean}) #, 'item_category_id': np.min})
    #train_features = train_features.join(items, on='item_id')
    #val_features = val_features.join(items, on='item_id')
    return train_features, val_features


def add_zip_feature(train_data, train_features, val_features, col1, col2, name):
    train_data[name] = list(zip(train_data[col1], train_data[col2]))
    train_features[name] = list(zip(train_features[col1], train_features[col2]))
    if val_features is not None:
        val_features[name] = list(zip(val_features[col1], val_features[col2]))


def generate_shop_item_features(train_data, train_features, val_features):
    add_zip_feature(train_data, train_features, val_features, 'shop_id', 'item_id', name='shop_item_id')

    mean_encoding(train_data, train_features, val_features, col='shop_item_id')

    add_zip_feature(train_data, train_features, val_features, 'shop_id', 'item_category_id', name='shop_icat_id')
    mean_encoding(train_data, train_features, val_features, col='shop_icat_id', target_col='item_price',
                  operation=np.min, name='min_price', lags=(3,), duration=3)
    mean_encoding(train_data, train_features, val_features, col='shop_icat_id', target_col='item_price',
                  operation=np.max, name='max_price', lags=(3,), duration=3)

    return train_features, val_features


class OldCache(Exception):
    pass


def generate_features(train_data, selection=None, cache=True):
    train_cache_name = 'data/train_features.h5'
    val_cache_name = 'data/val_features.h5'
    if cache:
        try:
            module_mod_time = os.path.getmtime(__file__)
            cache_mod_time = os.path.getmtime(train_cache_name)
            if module_mod_time > cache_mod_time:
                print('Features cache is old')
                raise OldCache()
            train_features = pd.read_hdf(train_cache_name)
            if selection is not None:
                val_features = pd.read_hdf(val_cache_name)
            else:
                val_features = None
            print('Loaded cached features')
            return train_features, val_features
        except:
            pass
    print('Generating training and validation features')
    if selection is not None:
        val_features = selection.copy()
    else:
        val_features = None
    train_features = train_data[['date_block_num', 'shop_id', 'item_id']].copy()
    train_features, val_features = generate_shop_features(train_data, train_features, val_features)
    train_features, val_features = generate_item_features(train_data, train_features, val_features)
    train_features, val_features = generate_shop_item_features(train_data, train_features, val_features)
    drop_columns = ['shop_item_id', 'shop_icat_id', 'date_block_num', 'item_id', 'shop_id']
    train_features.drop(columns=drop_columns, inplace=True)
    if val_features is not None:
        val_features.drop(columns=drop_columns, inplace=True)
        val_features.to_hdf(val_cache_name, 'w')

    train_features.to_hdf(train_cache_name, 'w')

    return train_features, val_features