import pandas as pd
import numpy as np
import argparse
import glob
import load_data
from crossvalidation import timeMonthSplit
from train import month_name


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--no_crossval', action='store_true')
    args = parser.parse_args()
    suffixes = ['rf1', 'rf2', 'xgb']
    weights = [0.5]*len(suffixes)
    weights = np.array(weights)
    weights /= sum(weights)

    # compute validation score for ensemble
    data = load_data.load_data()
    n_splits = 5
    rmses = []
    for train_idx, val_idx in timeMonthSplit(n_splits, data):
        Y_val = data['item_cnt_month'][val_idx]
        month = data[val_idx]['date_block_num'].min()
        agregg_pred = None
        for suffix, weight in zip(suffixes, weights):
            fname = 'data/predictions/valid_pred_' + str(month) + '_' + suffix + '.csv'
            Y_pred = np.loadtxt(fname)
            if agregg_pred is None:
                agregg_pred = Y_pred*weight
            else:
                agregg_pred += Y_pred*weight

        rmse = np.sqrt(((Y_val - agregg_pred) ** 2).mean())
        print("Date_block_num", month, month_name[month % 12])
        print('Valid RMSE ', rmse)
        rmses.append(rmse)

    rmse = np.mean(rmses)
    submissions = []
    aggreg_submission = None
    for suffix, weight in zip(suffixes, weights):
        submission_fname = glob.glob('data/submission_%s_*.csv' % suffix)[0]
        submissions.append(pd.read_csv(submission_fname, index_col=0))
        if aggreg_submission is None:
            aggreg_submission = submissions[-1]
            aggreg_submission['item_cnt_month'] *= weight
        else:
            aggreg_submission['item_cnt_month'] += submissions[-1]['item_cnt_month']*weight

    fname = 'data/submission_ensemble_%s_%f.csv' % ('-'.join(suffixes), rmse)
    aggreg_submission.to_csv(fname, index=True)


